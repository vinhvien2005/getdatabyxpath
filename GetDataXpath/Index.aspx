﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" EnableEventValidation="false" CodeBehind="Index.aspx.cs" Inherits="GetDataXpath.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tbody>
                <tr>
                    <td>URL</td>
                    <td><input type="text" id="txtUrl" style="width:600px" runat="server" /></td>
                </tr>
                <tr>
                    <td>User Agent</td>
                    <td><input type="text" id="txtUserAgent" style="width:600px" runat="server" /></td>
                </tr>
                <tr>
                    <td>Proxy</td>
                    <td><input type="text" id="txtProxy" style="width:600px" runat="server" /></td>
                </tr>
                <tr>
                    <td>Xpath</td>
                    <td><input type="text" style="width:600px" id="txtXpath" runat="server" /></td>
                </tr>
                <tr>
                    <td><input type="button" runat="server" onserverclick="submitData_click" value="Submit" /></td>
                    <td></td>
                </tr>
                <tr></tr>
                <tr>
                    <td>Result</td>
                    <td><textarea style="width:600px;height:400px" runat="server" id="txtResult"></textarea></td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
