﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Chrome;
using HtmlAgilityPack;

namespace GetDataXpath
{
    public partial class Index : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitData_click(object sender, EventArgs e)
        {
            try
            {

                ChromeOptions options_01 = new ChromeOptions();
                options_01.AddArgument(string.Format("--user-agent={0}", txtUserAgent.Value));
                Proxy pro = new Proxy();
                pro.Kind = ProxyKind.Manual;
                pro.IsAutoDetect = false;
                pro.HttpProxy = pro.SslProxy = txtProxy.Value;
                options_01.Proxy = pro;


                //var options = new PhantomJSOptions();
                //options.AddAdditionalCapability("phantomjs.page.settings.userAgent", txtUserAgent.Value);

                //var service = PhantomJSDriverService.CreateDefaultService();
                //service.ProxyType = "http";
                //service.Proxy = txtProxy.Value;
                //service.LoadImages = false;

                //IWebDriver driver = new PhantomJSDriver(service,options);
                IWebDriver driver = new ChromeDriver(options_01);

                driver.Navigate().GoToUrl(txtUrl.Value);
                var doc = new HtmlDocument();
                doc.LoadHtml(driver.PageSource);
                var data = doc.DocumentNode.SelectNodes(txtXpath.Value);
                if (data != null && data.Count > 0)
                {
                    txtResult.Value = data[0].InnerHtml;
                }
                else
                {
                    txtResult.Value = string.Format("Can't find data by Xpath value : {0}",txtXpath.Value);
                }

                driver.Quit();
                driver.Dispose();
            }
            catch (Exception ex)
            {
                txtResult.Value = string.Format("Server error : {0}",ex.Message);
            }
        }
    }
}